package taller.estructuras;

public class ColaPrioridad<T extends Comparable<T>>  {

	private int size;
	private NodoSencillo<T>[] pila;
	private int sizeMax;
	
	public ColaPrioridad(int tamanio){
		sizeMax = tamanio;
		pila[0]= null;
		
	}
	public void add(Object elemento) {
		// TODO Auto-generated method stub
		int n = size-1;
		NodoSencillo<T> x = new NodoSencillo<T>();
		x.setNodo((T) elemento);
		pila[n] = x;
		NodoSencillo<T> y = new NodoSencillo<T>();
		while(n>0 && pila[n-1].getNodo().compareTo(pila[n].getNodo())<0){
			y = pila[n-1];
			pila[n-1] = pila[n];
			pila[n] = y;
			n--;
		}
		size++;
		
	}

	public Object peek() {
		// TODO Auto-generated method stub
		return pila[0].getNodo();
	}

	public Object poll() {
		// TODO Auto-generated method stub
		T x = pila[0].getNodo();
		for(int i = 0; i<size;i++){
			pila[i] = pila[i+1];
		}
		size --;
		return x;
	}

	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size==0;
	}
	
	public int tamanioMax(){
		return sizeMax;
	}

	

	
}
